.PHONY: setup docs_html

setup:
	poetry config virtualenvs.create true
	poetry config virtualenvs.in-project true
	poetry install
	poetry run pre-commit install

docs_html:
	$(MAKE) -C docs html
