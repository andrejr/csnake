#!/usr/bin/env python3

import re
import subprocess
import sys
from pathlib import Path


def update_version(path):
    expected_version = (
        subprocess.check_output(
            ["poetry", "version", "--short", "--no-plugins"]  # noqa: S603,S607
        )
        .decode()
        .strip()
    )
    init_file = Path(__file__).parent / path
    with Path.open(init_file, "r+") as f:
        content = f.read()
        current_version = re.search(
            r"__version__\s*=\s*[\"']([^\"']+)[\"']", content
        ).group(1)
        if current_version != expected_version:
            print(
                f"Updating {init_file} from {current_version} to "
                f"{expected_version} to match Poetry version."
            )
            f.seek(0)
            f.write(
                re.sub(
                    r"(__version__\s=\s[\"'])[^'\"]+([\"'])",
                    lambda mtch: f"{mtch.group(1)}{expected_version}{mtch.group(2)}",
                    content,
                )
            )
            f.truncate()


if __name__ == "__main__":
    path = sys.argv[1]
    update_version(path)
